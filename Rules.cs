// Rules.cs: players read the rules.
RegisterPersistenceVar("readRules", false, "");

function serverCmdRules(%client) {
  if(%client.rulesCooked != 1)
    return;
  if(%client.lastRulesRead > (getSimTime() + 15000))
    return;
  %client.lastRulesRead = getSimTime();
  messageClient(%client, '', "\c5Rules:");
  messageClient(%client, '', "\c41a)\c6 Don't be a dick.");
  messageClient(%client, '', "\c41b)\c6 Also, do not use slurs nor slur derivatives. You aren't funny when you use them.");
  messageClient(%client, '', "\c42a)\c6 Do not spam in any way, shape, or form. This includes but isn't limited to: Denial of service, build spam, chat spam, Discord spam.");
  messageClient(%client, '', "\c42b)\c6 Provoked? Contact Acceleration#8086 or BL_ID 32440 (Acceleration Shark) and I will provide you with friendship to lean on. Abuse of my niceness will lead to very strict punishment.");
  messageClient(%client, '', "\c43)\c6 Swaying these rules using loopholes may lead to stricter punishments! Avoid that.");
  messageClient(%client, '', "\c5You will be given a hash to enter in chat in 10 seconds. Review the rules during this delay, not reading them is not an excuse for breaking them.");
  %client.readRules = 0;
  %client.schedule(10000, rulesHash);
}

function GameConnection::rulesHash(%client) {
  %client.readRulesHash = getSubStr(sha1(getRandom()), 0, 6);
  messageClient(%client, '', "\c3The hash to enter is \c5/rulesRead " @ %client.readRulesHash);
  return %client;
}

function GameConnection::haveYouRead(%client) {
  %client.rulesCooked = 1;
  if(%client.readRules != 1)
    messageClient(%client, '', "\c0You should read the \c5/rules\c0, not reading them is not an excuse for breaking them.");
}

function serverCmdRulesRead(%client, %hash) {
  if(%client.lastRulesRead2 > (getSimTime() + 3000) || %client.readRules)
    return;
  %client.lastRulesRead2 = getSimTime();
  if(%hash !$= %client.readRulesHash) {
    messageClient(%client, '', "Wrong hash, perhaps you should ask Acceleration Shark for help with using this command.");
    return;
  }
  
  messageClient(%client, '', "You have read the rules. Congrats :)");
  %client.readRules = 1;
}