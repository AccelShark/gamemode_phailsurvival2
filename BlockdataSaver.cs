// BlockdataSaver.cs: dynamic ore saver.

function BlockdataSaver_Load() {
  %fo = new FileObject(BlockdataSaver);
  %fo.openForRead("config/server/PhailS_Ores.txt");
  $PhailSTabs::Ores = 0;
  while(!%fo.isEOF()) {
    $PhailSTabs::Ore[$PhailSTabs::Ores++] = %fo.readLine();
  }
  if($PhailSTabs::Ores == 0) {
    warn("No PhailSurvival2 ores present. Will create a file with defaults.");
    $PhailSTabs::Ores = 0;
    // first field is uiName
    // second is colorID/colorFX/shapeFX
    // third is rarity/minDepth/maxDepth/hardness/XPgain
    $PhailSTabs::Ore[$PhailSTabs::Ores++] = "copper" TAB "50 1 0" TAB "0.1 -25 -250 25 2";
    $PhailSTabs::Ore[$PhailSTabs::Ores++] = "zinc" TAB "57 1 0" TAB "0.1 -25 -250 25 2";
    $PhailSTabs::Ore[$PhailSTabs::Ores++] = "tin" TAB "57 1 0" TAB "0.1 -250 -500 125 10";
  }
  %fo.close();
  %fo.openForRead("config/server/PhailS_Woods.txt");
  $PhailSTabs::Woods = 0;
  while(!%fo.isEOF()) {
    $PhailSTabs::Wood[$PhailSTabs::Woods++] = %fo.readLine();
  }
  if($PhailSTabs::Woods == 0) {
    warn("No PhailSurvival2 woods present. Will create a file with defaults.");
    // first field is uiName
    // second is colorID/colorFX/shapeFX
    // third is rarityfloor/hardness/XPgain
    $PhailSTabs::Woods = 0;
    $PhailSTabs::Wood[$PhailSTabs::Woods++] = "pine" TAB "39 0 0" TAB "25 2";
    $PhailSTabs::Wood[$PhailSTabs::Woods++] = "oak" TAB "51 0 0" TAB "125 10";
    $PhailSTabs::Wood[$PhailSTabs::Woods++] = "redwood" TAB "33 0 0" TAB "600 50";
  }
  BlockdataSaver_Save();
  echo("PhailSurvival2 BDSaver loaded " @ $PhailSTabs::Ores @ " ores and " @ $PhailSTabs::Woods @ " woods.");
  %fo.close();
  %fo.delete();
}

function BlockdataSaver_Save() {
  %fo = new FileObject(BlockdataSaver);
  %fo.openForWrite("config/server/PhailS_Ores.txt");
  for(%i = 1; %i <= $PhailSTabs::Ores; %i++) {
    %fo.writeLine($PhailSTabs::Ore[%i]);
  }
  %fo.close();
  %fo.openForWrite("config/server/PhailS_Woods.txt");
  for(%j = 1; %j <= $PhailSTabs::Woods; %j++) {
    %fo.writeLine($PhailSTabs::Wood[%j]);
  }
  echo("PhailSurvival2 BDSaver SAVED " @ %i-1 @ " ores and " @ %j-1 @ " woods.");
  %fo.close();
  %fo.delete();
}
