// Triangular.cs: triangular numbers
function mTriangularRoot(%hay) { return (msqrt((8*%hay)+1) - 1) / 2; }
function mTriangular(%needle) { return (%needle * (%needle+1)) / 2; }