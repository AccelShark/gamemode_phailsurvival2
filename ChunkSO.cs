// ChunkSO.cs: manage chunks of bricks
function ChunkSO::place(%chunk) {
  for(%x = 0; %x < $NoiseMapSO::Scale; %x++) {
    for(%y = 0; %y < $NoiseMapSO::Scale; %y++) {
      %gap = mFloatLength(getWord(%chunk.z, ((%x * $NoiseMapSO::Scale) + %y)), 0);
      %ag = mAbs(%gap);
      %gapSign = (%gap > -1 ?  1 : -1);
      for(%g = 0; mAbs(%g) <= %ag; %g += %gapSign) {
        %chunk.placeTopsoil((%chunk.x * ($NoiseMapSO::Scale)) + %x, (%chunk.y * ($NoiseMapSO::Scale)) + %y, %g);
      }
      if(getRandom() < 0.5 && (%x % 2 == 0 && %y % 2 == 0)) {
        //place tree
        // mFloatLength(mPow(getRandom() * 5, 8),0); is our coeff
        // it will be up by 3.2
        %isSpecial = 0;
        for(%i = 1; %i <= $PhailSTabs::WoodSpecials; %i++) {
          if(getRandom() < getField($PhailSTabs::WoodSpecial[%i], 2)) {
            %isSpecial = 1;
            break;
          }
        }
        if(!%isSpecial) { %i = getRandom(1, $PhailSTabs::Woods); }
        %cattrs = getField((%isSpecial ? "0 0 0" : $PhailSTabs::Wood[%i]), 1);
        %tree = new fxDTSBrick() {
          datablock = "brickPineTreeData";
          position = ((%chunk.x * ($NoiseMapSO::Scale) + %x) * 2)  SPC ((%chunk.y * ($NoiseMapSO::Scale) + %y) * 2) SPC (((12500 + %ag) * 2) + 3.2);
          rotation = "0 0 0 0";
          colorID = getWord(%cattrs, 0);
          scale = "1 1 1";
          angleID = "0";
          colorfxID = getWord(%cattrs, 1);
          shapefxID = getWord(%cattrs, 2);
          isPlanted = 1;
          stackBL_ID = 888888;
          phailT = (%isSpecial ? $PhailSTypes::WoodSpecial : $PhailSTypes::Wood);
          phailT_sub = %i;
          phailD = 0;
          phailD_totalParties = 0; // 888888 just signifies indirect D
          phailO = $PhailSObjects::Wood;
        };
        if(!%isSpecial) {
          %tree.phailD_max = getWord(getField($PhailSTabs::Wood[%i], 2), 0);
        } else {
          // Schedule by 100 so we can have the time to add to Public Brickgroup
          $DefaultMiniGame.schedule(0, getField($PhailSTabs::WoodSpecial[%i], 1), 0, %tree, 0);
        }
        BrickGroup_888888.add(%tree);
        %tree.plant();
        %tree.setTrusted(1);
        %tree.onPlant();
        
      }
    }
  }
  return %chunk;
}

function ChunkSO::placeTopsoil(%chunk, %x, %y, %z) {
  %brick = new fxDTSBrick() {
    datablock = "brick4xCubeData";
    position = (%x * 2) SPC (%y * 2) SPC ((12500 + %z) * 2);
    rotation = "0 0 0 0";
    colorID = 41 + (%z % 6);
    scale = "1 1 1";
    angleID = "0";
    colorfxID = 0;
    shapefxID = 0;
    isPlanted = 1;
    stackBL_ID = 888888;
    phailT = $PhailSTypes::Topsoil;
    phailD = 0;
    phailD_totalParties = 0; // 888888 just signifies indirect D
    phailD_max = 5;
    phailO = $PhailSObjects::Ore;
  };
  
  BrickGroup_888888.add(%brick);
  %brick.plant();
  %brick.setTrusted(1);
  %brick.onPlant();
  return %brick;
}

function ChunkSO::placeSpawns(%chunk) {
  for(%x = 0; %x < 4; %x++) {
    for(%y = 0; %y < 4; %y++) {
      %brick = new fxDTSBrick() {
        datablock = "brickSpawnPointData";
        // Bicubic = prone to overshoot Q so we must set that to 5x maxHeight
        // In addition we will add trees and stuff so that'll be fun.
        position = (%chunk.x * 8) + (%x * 2) SPC (%chunk.y * 8) + (%y * 2) SPC (25002.5 + (5 * $NoiseMapSO::MaxHeight));
        rotation = "0 0 0 0";
        colorID = 16;
        scale = "1 1 1";
        angleID = "0";
        colorfxID = 0;
        shapefxID = 0;
        isPlanted = 1;
        stackBL_ID = 888888;
        phailT = $PhailSTypes::Indestructible;
      };
      
      BrickGroup_888888.add(%brick);
      %brick.plant();
      %brick.setTrusted(1);
      %brick.onPlant();
    }
  }
  return %chunk;
}

function ChunkSO::onAdd(%chunk) {
  %chunk.place();
  $DefaultMiniGame.chunkK[$DefaultMiniGame.chunks.getCount()] = %chunk.x SPC %chunk.y;
  $DefaultMiniGame.chunkV[%chunk.x SPC %chunk.y] = $DefaultMiniGame.chunks.getCount();
  $DefaultMiniGame.chunks.add(%chunk);
  return %chunk;
}