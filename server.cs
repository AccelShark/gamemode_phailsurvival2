// server.cs: strap up the server then execute what is needed.
RegisterPersistenceVar("phailStat", false, "");
RegisterPersistenceVar("phailL", false, "");
RegisterPersistenceVar("phailX", false, "");
RegisterPersistenceVar("phailPersonal", false, "");


if($GameModeArg !$= "Add-Ons/Gamemode_PhailSurvival2/gamemode.txt") {
  error("Error: Gamemode_PhailSurvival2 cannot be used in custom games!");
  return;
}
exec("./Rules.cs");
exec("./ChunkSO.cs");
exec("./NoiseMapSO.cs");
exec("./Bicubic.cs");
exec("./Triangular.cs");
exec("./BlockdataSaver.cs");

// Noisemap Parameters, don't change.
$NoiseMapSO::Scale = 8;
$NoiseMapSO::MaxHeight = 6;
// Don't change these either.
$PhailSVersion = 1;
$PhailSTypes::Topsoil = 1;
$PhailSTypes::Water = 2;
$PhailSTypes::Ore = 3;
$PhailSTypes::Special = 4;
$PhailSTypes::Indestructible = 5;
$PhailSTypes::Dirt = 6;
$PhailSTypes::Wood = 7;
$PhailSTypes::WoodSpecial = 8;
$PhailSObjects::Ore = 1;
$PhailSObjects::Wood = 2;
$PhailSHitscanTypes::Direct = 1;
$PhailSHitscanTypes::Indirect = 2;
$PhailSHitscanFlood = 200;

$PhailSTabs::Dirt[0] = "upper dirt" TAB "45 0 0" TAB "5";
$PhailSTabs::Dirt[1] = "middle dirt" TAB "43 0 0" TAB "10";
$PhailSTabs::Dirt[2] = $PhailSTabs::Dirt[1];
$PhailSTabs::Dirt[3] = "lower dirt" TAB "58 0 0" TAB "25";
$PhailSTabs::Dirt[4] = "bedrock" TAB "62 1 0" TAB "50";
$PhailSTabs::Dirt[5] = "upper mantle" TAB "33 0 0" TAB "100";
$PhailSTabs::Dirt[6] = "middle mantle" TAB "0 0 0" TAB "250";
$PhailSTabs::Dirt[7] = $PhailSTabs::Dirt[6];
$PhailSTabs::Dirt[8] = "lower mantle" TAB "1 3 0" TAB "500";
$PhailSTabs::Dirt[9] = "outer core" TAB "2 3 2" TAB "1000";
$PhailSTabs::Dirt[10] = "middle core" TAB "10 3 2" TAB "2500";
$PhailSTabs::Dirt[11] = "inner core" TAB "56 3 2" TAB "5000";
$PhailSTabs::Dirt[12] = "broken code" TAB "0 3 1" TAB "50000";

$PhailSTabs::Specials = 0;
$PhailSTabs::Special[$PhailSTabs::Specials++] = "chance block" TAB "onChanceEvent" TAB "0.005";

$PhailSTabs::WoodSpecials = 0;
$PhailSTabs::WoodSpecial[$PhailSTabs::WoodSpecials++] = "chance tree" TAB "onChanceTreeEvent" TAB "0.005";


function MiniGameSO::onChanceEvent(%minigame, %ev, %block, %client) {
  // 0 = on place
  // 1 = on hit
  // 2 = on destroy
  switch(%ev) {
    case 0:
      %depth = getWord(%block.position, 2) - 25000;
      %block.colorID = 23;
      %block.colorFXID = 3;
      %block.phailT = $PhailSTypes::Special;
      %block.phailT_sub = 1;
      %block.phailD = 0;
      %block.phailD_totalParties = 0; // 888888 just signifies indirect D
      %block.phailD_max = mFloatLength(mPow(%depth / 100, 2), 0);
      return %minigame;
    case 1: return %minigame;
    case 2:
      %random = getRandom(0, 1);
      switch(%random) {
        case 0:
          messageClient(%client, '', "The chance block changes your Mining XP.");
          %sh = (%block.phailD_max * mFloatLength((getRandom() * 2) - 1, 1));
          %client.adjPhailXP(0, %sh);
          return %minigame;
        case 1:
          if(getWord(%client.phailPersonal, 0) == 0 && getRandom() < 0.1) {
            messageClient(%client, '', "The chance block contains an odd object which seems to be a new ore!");
            %depth = mAbs(getWord(%block.position, 2) - 25000);
            %orename = strreplace(strlwr(%client.name), " ", "") @ (getRandom() > 0.5 ? "ium" : "ite");
            %orelvl = %client.getPhailLevel(0);
            $PhailSTabs::Ore[$PhailSTabs::Ores++] = %orename TAB getRandom(0,63) SPC "0 0" TAB "0.01 " @ %depth @ " -12500 " @ (%orelvl * 5) SPC %orelvl;
            serverPlay2D(rewardSound);
            messageAll('MsgAdminForce', %client.name @ " discovered a new ore.");
            %client.phailPersonal = setWord(%client.phailPersonal, 0, $PhailSTabs::Ores);
            BlockdataSaver_Save();
          } else { messageClient(%client, '', "The chance block slumbers."); }
          return %minigame;
    }
  }
}

function MiniGameSO::onChanceTreeEvent(%minigame, %ev, %block, %client) {
  // 0 = on place
  // 1 = on hit
  // 2 = on destroy
  switch(%ev) {
    case 0:
      %block.colorID = 23;
      %block.colorFXID = 3;
      %block.phailT = $PhailSTypes::WoodSpecial;
      %block.phailT_sub = 1;
      %block.phailD_max = mFloatLength(mPow(getRandom() * 5, 7), 0);
      return %minigame;
    case 1: return %minigame;
    case 2:
      %random = getRandom(0, 1);
      switch(%random) {
        case 0:
          messageClient(%client, '', "The chance tree changes your Woodcutting XP.");
          %sh = (%block.phailD_max * mFloatLength((getRandom() * 2) - 1, 1));
          %client.adjPhailXP(1, %sh);
          return %minigame;
        case 1:
          if(getWord(%client.phailPersonal, 1) == 0 && getRandom() < 0.1) {
            messageClient(%client, '', "The chance tree seems odd, but it's a new species!");
            %depth = mAbs(25000 + getWord(%block.position, 2));
            %treename = strreplace(strlwr(%client.name), " ", "") @ (getRandom() > 0.5 ? "a" : "wood");
            %treelvl = %client.getPhailLevel(1);
            $PhailSTabs::Wood[$PhailSTabs::Woods++] = %treename TAB getRandom(0, 63) SPC "0 0" TAB (%treelvl * 5) SPC %treelvl;
            serverPlay2D(rewardSound);
            messageAll('MsgAdminForce', %client.name @ " discovered a new tree.");
            %client.phailPersonal = setWord(%client.phailPersonal, 1, $PhailSTabs::Woods);
            BlockdataSaver_Save();
          } else { messageClient(%client, '', "The chance tree withers."); }
          return %minigame;
    }
  }
}

function MiniGameSO::onTimeTrigger(%minigame) {
  if(%minigame.ticksElapsed % 2400 == 0) {
    messageAll('', "BlockdataSaver is auto-saving ores...");
    BlockdataSaver_Save();
  }
  if(getBrickCount() > $Server::GhostLimit - 2500 || %minigame.isClearing) {
    if(!%minigame.isClearing) {
      %minigame.isClearing = 1;
      messageAll('MsgAdminForce', "Clearing bricks in 5 seconds due to excess bricks.");
      %minigame.clearSchedule = %minigame.schedule(5000, "performClear");
    } else if(%minigame.ticksElapsed % 50 == 0) {
      if(getBrickCount() == 0) {
        %minigame.isClearing = 0;
        %minigame.schedule(500, "placeChunkSpawns");
        %minigame.schedule(1000, "reset", 0);
      }
      %minigame.chatMessageAll('', "There are " @ getBrickCount() @ "/" @ $Server::GhostLimit @ " bricks left to be cleared.");
    }
    return %minigame.clearSchedule;
  }
  for(%m = 0; %m < %minigame.numMembers; %m++) {
    %mm = %minigame.member[%m];
    for(%i=0; %i<2; %i++) {
      %avg += getWord(%mm.phailL, %i);
    }
    %avg /= 2;
    %mm.setscore(mFloor(%avg));
    if(!isObject(%mm.player))
      continue; // oops.
    if(%mm.player.triggeredPosPhail $= "" || %minigame.ticksElapsed % 10 == 0)
      %mm.player.triggeredPosPhail = %mm.player.getPosition();
    %mm.bottomPrint("<just:center>\c5[LV] \c4M: \c6" @ getWord(%mm.phailL, 0) SPC "\c4W: \c6" @ getWord(%mm.phailL, 1) SPC "\c4S: \c6" @ getWord(%mm.phailL, 2) SPC "\c4L: \c6" @ getWord(%mm.phailL, 3) NL "\c5[XP] \c4M: \c6" @ getWord(%mm.phailX, 0) SPC "\c4W: \c6" @ getWord(%mm.phailX, 1) SPC "\c4S: \c6" @ getWord(%mm.phailX, 2) SPC "\c4L: \c6" @ getWord(%mm.phailX, 3) NL "\c5Bearing {\c4" @ VectorScale(VectorSub(%mm.player.triggeredPosPhail, "0 0 25001"), 0.5) @ "\c5}", 2, 1);
    %v0 = strreplace(VectorAdd("-0.5 -0.5 0", VectorScale(%mm.player.getPosition(), 0.5 / $NoiseMapSO::Scale)), "-0", "0");
    %v1x = mFloatLength(getWord(%v0, 0), 0);
    %v1y = mFloatLength(getWord(%v0, 1), 0);
    %v1 = %v1x SPC %v1y SPC "0";
    if(VectorDist(%v0, %v1) > 0.1) {
      for(%i = -1; %i < 2; %i++) {
        for(%j = -1; %j < 2; %j++) {
          if(%minigame.chunkV[%v1x + %i SPC %v1y + %j] $= "") {
            NoiseMapSO.chunkWhiteNoise(%v1x + %i , %v1y + %j);
          }
        }
      }
    }
  }
  return %minigame;
}

function MiniGameSO::dissociateChunks(%minigame) {
  messageAll('', "BlockdataSaver is saving ores...");
  BlockdataSaver_Save();
  %cnt = $DefaultMiniGame.chunks.getCount();
  for(%i = 0; %i < %cnt; %i++) {
    %k = $DefaultMiniGame.chunkK[%i];
    $DefaultMiniGame.chunkK[%i] = "";
    $DefaultMiniGame.chunkV[%k] = "";
  }
  ChunkSG.deleteAll();
  messageAll('', "Chunks dissociated.");
  %minigame.noisemap.delete();
  %minigame.noisemap = new ScriptObject(NoiseMapSO) {
    scale = $NoiseMapSO::Scale;
    maxHeight = $NoiseMapSO::MaxHeight;
  };
  messageAll('', "NoiseMap reallocated.");
  return %minigame;
}

function MiniGameSO::performClear(%minigame) {
  %minigame.isClearing = 1;
  messageAll('MsgClearBricks', "A brickgroup clear has been queued.");
  BrickGroup_888888.chainDeleteAll();
  messageAll('', "Dissociating chunks... (this will lag)");
  %minigame.schedule(1000, "dissociateChunks");
  return %minigame;
}

function MiniGameSO::timeTrigger(%minigame) {
  %minigame.onTimeTrigger();
  %minigame.ticksElapsed++;
  %minigame.nextTimeTrigger = %minigame.schedule(100, "timeTrigger");
  return %minigame;
}

function MiniGameSO::placeChunkSpawns(%minigame) {
  %minigame.noisemap.chunkWhiteNoise(0, 0);
  %minigame.chunks.getObject(%minigame.chunkV["0 0"]).placeSpawns();
  return %minigame;
}

function fxDTSBrick::getNamePhail(%obj) {
  switch(%obj.phailT) {
    case $PhailSTypes::Topsoil:
      return "topsoil";
    case $PhailSTypes::Ore:
      return getField($PhailSTabs::Ore[%obj.phailT_sub], 0);
    case $PhailSTypes::Special:
      return getField($PhailSTabs::Special[%obj.phailT_sub], 0);
    case $PhailSTypes::Dirt:
      return getField($PhailSTabs::Dirt[%obj.phailT_sub], 0);
    case $PhailSTypes::Wood:
      return getField($PhailSTabs::Wood[%obj.phailT_sub], 0);
    case $PhailSTypes::WoodSpecial:
      return getField($PhailSTabs::WoodSpecial[%obj.phailT_sub], 0);
    default:
      return "UNIMPLEMENTED!";
  }
}

function GameConnection::getPhailLevel(%client, %what) {
  return getWord(%client.phailL, %what);
}

function Player::getPhailLevel(%obj, %client, %what) {
  if(isObject(%client)) {
    return %client.getPhailLevel(%what);
  } else {
    return getWord(%obj.phailL, %what);
  }
}

function GameConnection::adjPhailLevel(%client, %what, %levelAdj) {
  %adj = getWord(%client.phailL, %what) + %levelAdj;
  %client.phailL = setWord(%client.phailL, %what, %adj);
  return %client;
}

function Player::adjPhailLevel(%obj, %client, %what, %levelAdj) {
  if(isObject(%client)) {
    return %client.adjPhailLevel(%what, %levelAdj);
  }
  return %obj;
}

function GameConnection::adjPhailXP(%client, %what, %xpAdj) {
  %xp = getWord(%client.phailX, %what);
  %lvl = getWord(%client.phailL, %what);
  %lvlCoeff = mTriangular(%lvl) * 10;
  %xpCoeff = mFloatLength((mTriangular(%lvl-1) * 10) + %xp + %xpAdj, 0);
  if(%xpCoeff > %lvlCoeff) {
    // Calculate based on summations (see Triangular.cs)
    %newLvl = mFloor(mTriangularRoot(%xpCoeff / 10)) + 1;
    %newXP = mFloor(%xpCoeff - %lvlCoeff);   
    %client.phailX = setWord(%client.phailX, %what, %newXP);
    %client.phailL = setWord(%client.phailL, %what, %newLvl);
    return %client;
  } else if(%xpCoeff < mTriangular(%lvl - 1) * 10) {
    %newLvl = mFloor(mTriangularRoot(%xpCoeff / 10)) - 1;
    %newXP = mFloor(%xpCoeff + %lvlCoeff);
    talk("Negative XP yields UNIMPLEMENTED. New L: " @ %newLvl SPC "X: " @ %newXP);
    return %client;
    %client.phailX = setWord(%client.phailX, %what, %newXP);
    %client.phailL = setWord(%client.phailL, %what, %newLvl);
  }
  %client.phailX = setWord(%client.phailX, %what, mFloor(%xp + %xpadj));
  return %client;
}

function Player::adjPhailXP(%obj, %client, %what, %xpAdj) {
  if(isObject(%client)) { %client.adjPhailXP(%what, %xpAdj); }
  return %obj;
}

function fxDTSBrick::placeDirtPhail(%obj, %pos) {
  %rpos = vectorAdd(%obj.position, %pos);
  if($DefaultMiniGame.noisemap.neighborRemnants[%rpos]) { return; }
  for(%i=1; %i<=$PhailSTabs::Ores; %i++) {
    %tabc = getField($PhailSTabs::Ore[%i], 1);
    %tab2 = getField($PhailSTabs::Ore[%i], 2);
    %d = (getWord(%rpos, 2)-25000) / 2;
    if(getWord(%tab2, 1) < %d || getWord(%tab2, 2) > %d) { continue; }
    if(getRandom() < getWord(%tab2, 0)) {
      %brick = new fxDTSBrick() {
        datablock = "brick4xCubeData";
        position = %rpos;
        rotation = "0 0 0 0";
        colorID = getWord(%tabc, 0);
        scale = "1 1 1";
        angleID = "0";
        colorfxID = getWord(%tabc, 1);
        shapefxID = getWord(%tabc, 2);
        isPlanted = 1;
        stackBL_ID = 888888;
        phailT = $PhailSTypes::Ore;
        phailT_sub = %i;
        phailD = 0;
        phailD_totalParties = 0; // 888888 just signifies indirect D
        phailD_max = getWord(%tab2, 3);
        phailO = $PhailSObjects::Ore;
      };
      $DefaultMiniGame.noisemap.neighborRemnants[%rpos] = 1;
      BrickGroup_888888.add(%brick);
      %brick.plant();
      %brick.setTrusted(1);
      %brick.onPlant();
      return %obj;
    }
  }
  for(%i=1; %i<=$PhailSTabs::Specials; %i++) {
    %tab2 = getField($PhailSTabs::Special[%i], 2);
    %d = (getWord(%rpos, 2)-25000) / 2;
    if(getRandom() < getWord(%tab2, 0)) {
      %brick = new fxDTSBrick() {
        datablock = "brick4xCubeData";
        position = %rpos;
        rotation = "0 0 0 0";
        colorID = 0;
        scale = "1 1 1";
        angleID = "0";
        colorfxID = 0;
        shapefxID = 0;
        isPlanted = 1;
        stackBL_ID = 888888;
        phailT = $PhailSTypes::Special;
        phailO = $PhailSObjects::Ore;
      };
      $DefaultMiniGame.noisemap.neighborRemnants[%rpos] = 1;
      BrickGroup_888888.add(%brick);
      %brick.plant();
      %brick.setTrusted(1);
      %brick.onPlant();
      $DefaultMiniGame.schedule(0, getField($PhailSTabs::Special[%i], 1), 0, %brick, 0);
      return %obj;
    }
  }
  
  %thisDirt = 12-mFloatLength(((getWord(%rpos, 2) / 2000)), 0);
  %colattr = getField($PhailSTabs::Dirt[%thisDirt], 1);
  %brick = new fxDTSBrick() {
    datablock = "brick4xCubeData";
    position = %rpos;
    rotation = "0 0 0 0";
    colorID = getWord(%colattr, 0);
    scale = "1 1 1";
    angleID = "0";
    colorfxID = getWord(%colattr, 1);
    shapefxID = getWord(%colattr, 2);
    isPlanted = 1;
    stackBL_ID = 888888;
    phailT = $PhailSTypes::Dirt;
    phailT_sub = %thisDirt;
    phailD = 0;
    phailD_totalParties = 0; // 888888 just signifies indirect D
    phailD_max = getField($PhailSTabs::Dirt[%thisDirt], 2);
    phailO = $PhailSObjects::Ore;
  };
  
  $DefaultMiniGame.noisemap.neighborRemnants[%rpos] = 1;
  BrickGroup_888888.add(%brick);
  %brick.plant();
  %brick.setTrusted(1);
  %brick.onPlant();
  return %obj;
}

function fxDTSBrick::tryBreakPhail(%obj, %breaker, %type, %intensity) {
  if(%minigame.isClearing) { return %obj; }
  %bcl = %breaker.client;
  if(isObject(%bcl)) {
    if(%obj.phailD_totalParties > 0) {
      for(%p = 0; %p < %obj.phailD_totalParties; %p++) {    
        if(%obj.phailD_partyBLID[%p] == %bcl.getBLID()) {
          %thisp = %p;
          break;
        }
      }
    }
    if(%thisp $= "") {
      %thisp = %obj.phailD_totalParties;
      %obj.phailD_totalParties++;
      %obj.phailD_partyBLID[%thisp] = %bcl.getBLID();
    }
    %msgBcl = 1;
  } else {
    if(%obj.phailD_totalParties > 0) {
      for(%p = 0; %p < %obj.phailD_totalParties; %p++) {    
        if(%obj.phailD_partyBLID[%p] == 888888) {
          %thisp = %p;
          break;
        }
      }
    }
    if(%thisp $= "") {
      %thisp = %obj.phailD_totalParties;
      %obj.phailD_totalParties++;
      %obj.phailD_partyBLID[%thisp] = 888888;
    }
  }
  switch(%obj.phailO) {
  
    // MINING
    
    case $PhailSObjects::Ore:
      %obj.playSound(hammerHitSound);
      %breakD = mFloatLength(%breaker.getPhailLevel(%bcl, 0) * %intensity, 0);
      %newBreak = %breakD + %obj.phailD;
      if(%newBreak > %obj.phailD_max) {
        %obj.playSound(brickBreakSound);
        if(%msgBcl) {
          %bcl.centerPrint("Mined " @ %obj.getNamePhail(), 2);
        }
        %dmnm = $DefaultMiniGame.noisemap;
        if(%obj.phailT != $PhailSTypes::Topsoil) {
          %obj.placeDirtPhail("-2 0 0");
          %obj.placeDirtPhail("2 0 0");
          %obj.placeDirtPhail("0 -2 0");
          %obj.placeDirtPhail("0 2 0");
          %obj.placeDirtPhail("0 0 -2");
          if(getWord(%obj.position, 2) != 24998)
            %obj.placeDirtPhail("0 0 2");
        } else if(getWord(%obj.position, 2) == 25000) {
          %obj.placeDirtPhail("0 0 -2");
        }
        if(%obj.phailT == $PhailSTypes::Ore) {
          %bcl.adjPhailXP(0, getWord(getField($PhailSTabs::Ore[%obj.phailT_sub], 2), 4));
        } if(%obj.phailT == $PhailSTypes::Special) {
          $DefaultMiniGame.schedule(0, getField($PhailSTabs::Special[%obj.phailT_sub], 1), 2, %obj, %bcl);
        }
        %obj.schedule(100,delete);
      } else {
        if(%msgBcl) {
          if(%obj.phailT == $PhailSTypes::Special) {
            $DefaultMiniGame.schedule(0, getField($PhailSTabs::Special[%obj.phailT_sub], 1), 1, %obj, %bcl);
          }
          %bcl.centerPrint("Mining " @ %obj.getNamePhail() NL "\c4HP\c5: " @ %obj.phailD_max - %obj.phailD @ "\c4/\c5" @ %obj.phailD_max, 2);
        }
        %obj.phailD = %newBreak;
      }
      return %obj;

    // WOODCUTTING

    case $PhailSObjects::Wood:
      %obj.playSound(swordHitSound);
      %breakD = mFloatLength(%breaker.getPhailLevel(%bcl, 1) * %intensity, 1);
      %newBreak = %breakD + %obj.phailD;
      if(%newBreak > %obj.phailD_max) {
        %obj.playSound(brickBreakSound);
        if(%msgBcl) {
          %bcl.centerPrint("Chopped down " @ %obj.getNamePhail(), 2);
        }
        if(%obj.phailT == $PhailSTypes::Wood) {
          %bcl.adjPhailXP(1, getWord(getField($PhailSTabs::Wood[%obj.phailT_sub], 2), 1));
        } if(%obj.phailT == $PhailSTypes::WoodSpecial) {
          $DefaultMiniGame.schedule(0, getField($PhailSTabs::WoodSpecial[%obj.phailT_sub], 1), 2, %obj, %bcl);
        }
        %obj.schedule(100,delete);
      } else {
        if(%msgBcl) {
          if(%obj.phailT == $PhailSTypes::WoodSpecial) {
            $DefaultMiniGame.schedule(0, getField($PhailSTabs::WoodSpecial[%obj.phailT_sub], 1), 1, %obj, %bcl);
          }
          %bcl.centerPrint("Chopping down " @ %obj.getNamePhail() NL "\c4HP\c5: " @ %obj.phailD_max - %obj.phailD @ "\c4/\c5" @ %obj.phailD_max, 2);
        }
        %obj.phailD = %newBreak;
      }
      return %obj;
  }
}

function Player::hitScanPhail(%obj, %val, %slot) {
  if(%val && (%obj.currTool $= "" || %obj.currTool == -1) && !isEventPending(%obj.isHittingPhail) && (%obj.hitThrottle < getSimTime())) {
    %nph = %obj.gethackposition();
    %npe = vectorscale(%obj.getMuzzleVector(%slot), 5);
    %next = containerRaycast(%nph, VectorAdd(%nph, %npe), $TypeMasks::FxBrickObjectType);
    %obj.isHittingPhail = %obj.schedule($PhailSHitscanFlood, hitScanPhail, %val);
    %nextObj = getWord(%next, 0);
    if(isObject(%nextObj) && (%nextObj.phailT != $PhailSTypes::Indestructible) && %nextObj.phailT > 0) {
      %obj.playThread(3, activate);
      %nextObj.tryBreakPhail(%obj, $PhailSHitscanTypes::Direct, 1.0);
    }
  } else {
    if(isEventPending(%obj.isHittingPhail)) {
      if(!%val) { %obj.hitThrottle = getSimTime() + $PhailSHitscanFlood; }
      cancel(%obj.isHittingPhail);
      %obj.isHittingPhail = -1;
    }
  }
  return %obj.isHittingPhail;
}

function GameConnection::doStatsPhail(%client) {
  if(%client.phailStat $= "") {
    messageClient(%client, '', "\c4You don't seem to have a save file. A little here and a little there and we'll get you all taken care of!");
    $DefaultMiniGame.chatMessageAll('', "\c5--- === ---");
    %client.phailStat = $PhailSVersion;
    %client.phailL = "1 1 1 1";
    %client.phailX = "0 0 0 0";
  } else if(%client.phailStat < $PhailSVersion) {
    messageClient(%client, '', "\c4You don't seem to have an up-to-date save file. Migration will be implemented soon.");
  }
}

package PhailSurvival2 {
  function MiniGameSO::onAdd(%minigame) {
    // Return parent
    BlockdataSaver_Load();
    %parent = parent::onAdd(%minigame);
    %minigame.ticksElapsed = 0;
    %minigame.chunks = new ScriptGroup(ChunkSG);
    %minigame.noisemap = new ScriptObject(NoiseMapSO) {
      scale = $NoiseMapSO::Scale;
      maxHeight = $NoiseMapSO::MaxHeight;
    };
    %minigame.schedule(1000, "placeChunkSpawns");
    %minigame.timeTrigger();
    return %parent;
  }
  
  // triggerNum[0=activate,1=use,2=jump,3=crouch,4=jet]
  function armor::onTrigger(%self, %obj, %triggerNum, %val) {
    switch(%triggerNum) {
      case 0:
        %parent = parent::onTrigger(%self, %obj, %triggerNum, %val);
        %obj.hitScanPhail(%val, %slot);
        return %parent;
      default:
        %parent = parent::onTrigger(%self, %obj, %triggerNum, %val);
        return %parent;
    }
  }
  function MiniGameSO::addMember(%minigame, %client) {
    %parent = parent::addMember(%minigame, %client);
    %client.schedule(500, "applyPersistence");
    %client.schedule(1000, "doStatsPhail");
    %client.schedule(1500, "haveYouRead");
    return %parent;
  }
};

deactivatePackage(PhailSurvival2);
activatePackage(PhailSurvival2);