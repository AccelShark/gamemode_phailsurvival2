// NoiseMapSO.cs: white noise map smoothed with bicubic splines
function NoiseMapSO::chunkWhiteNoise(%noise, %x, %y) {
    // do noise
  %rand = getRandom();
  for(%i = -1; %i < 3; %i++) {
    for(%j = -1; %j < 3; %j++) {
      if(%noise.height[%x + %i, %y + %j] $= "") {
        %noise.height[%x + %i, %y + %j] = %noise.maxHeight * %rand;
      }
      %map = %map SPC %noise.height[%x + %i, %y + %j];
    }    
  }
  %map = ltrim(%map);
  %imap = interpolateBicubic(%map, %noise.scale);
  if($DefaultMiniGame.chunkV[%x SPC %y] !$= "") { return %noise; }
  
  new ScriptObject(ChunkSO) {
    x = %x;
    y = %y;
    z = %imap;
  };
  return %noise;
}